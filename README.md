# MhhCommon

[![CI Status](http://img.shields.io/travis/Mathias H. Hansen/MhhCommon.svg?style=flat)](https://travis-ci.org/Mathias H. Hansen/MhhCommon)
[![Version](https://img.shields.io/cocoapods/v/MhhCommon.svg?style=flat)](http://cocoapods.org/pods/MhhCommon)
[![License](https://img.shields.io/cocoapods/l/MhhCommon.svg?style=flat)](http://cocoapods.org/pods/MhhCommon)
[![Platform](https://img.shields.io/cocoapods/p/MhhCommon.svg?style=flat)](http://cocoapods.org/pods/MhhCommon)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MhhCommon is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MhhCommon'
```

## Author

Mathias H. Hansen, dev@mathiashh.dk

## License

MhhCommon is available under the MIT license. See the LICENSE file for more info.
