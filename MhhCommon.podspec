#
# Be sure to run `pod lib lint MhhCommon.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MhhCommon'
  s.version          = '0.5.0'
  s.summary          = 'MhhCommon is a library that shares code that is common for the Mhh libs.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "MhhCommon is a library that shares code that is common for the Mhh libs. This is longer than the summary text."

  s.homepage         = 'https://bitbucket.org/MathiasHH/MhhCommon'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mathias H. Hansen' => 'dev@mathiashh.dk' }
  s.source           = { :git => 'https://bitbucket.org/MathiasHH/MhhCommon.git', :tag => s.version.to_s }
  #s.source           = { :git => 'https://bitbucket.org/MathiasHH/MhhCommon.git', :commit => "de65d23" }

  s.ios.deployment_target = '10.0'

  s.source_files = 'MhhCommon/Classes/**/*'
  
end
