//  
//  Auth.swift
//  MhhCommon
//
//  Created by Mathias H. Hansen on 22/01/2018.
//
//  Copyright 2017-2018 Mathias Hedemann Hansen.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import Foundation

/// TODO write documentation
public protocol Auth {
	func login(
		with email: String,
		and password: String,
		callback: @escaping (Error?, User?) -> Void
	)
	func logout(callback: ((Error?, Bool) -> Void)?)
	func resetEmail(
		email: String,
		callback: ((Error?) -> Void)?
	)
}

/// TODO write documentation
public protocol AuthModel: Auth {
	var rules: AuthRules? { get }
	func tryToRegisterUser(
		with email: String,
		and password: String,
		and confirmPassword: String,
		callback: @escaping (Error?, Any?) -> Void
	)
	func sendVerificationEmail(
		email: String,
		password: String,
		callback: @escaping (Error?, User?) -> Void
	)
	func tryToChangeEmail(
		currentEmail: String,
		newEmail: String,
		password: String,
		callback: (Error?) -> Void
	)
}

/// TODO write documentation
public protocol AuthNetwork: Auth {
	func createUser(
		with email: String,
		and password: String,
		callback: @escaping (Error?, User?) -> Void
	)
	func sendVerificationEmail(user: User)
	func changeEmail(
		newEmail: String,
		callback: (Error?) -> Void
	)
}

public protocol AuthRules {
	func isPasswordEqualToConfirmPassword(password: String, confirmPassword: String) -> Bool
	func isPasswordValid(_ password: String) -> Bool
}


/// TODO write documentation
public enum AuthError: Error {
	
	case emailNotVerified
	case emailAlreadyVerified
	
	public var simpleDescription: String {
		switch self {
		  case .emailNotVerified:
			  return "Email address is not verified"
		  case .emailAlreadyVerified:
			  return "Email address is already verified"
		}
	}
}

//public protocol LoginRules: AuthRules {
//}
//public protocol SignupRules: AuthRules {
//}
